#/usr/bin/python
# -*- coding:utf-8 -*-

import serial;
import serial.tools.list_ports;
import struct, time;
from array import array;
from enum import Enum;

SERPATH = "/dev/ttyACM0";
SERBPS = 115200;

def getPCserial():
    port_list = list(serial.tools.list_ports.comports());
    return port_list;

SendCMD = Enum('SendCMD', (
    'getName', 'ioSize', 'init', "setMode",
    'setLevel', 'getLevel'));
IOName  = Enum('IOName', (
    'PA0', 'PA1', 'PA2', 'PA3', 'PA4', 'PA5', 'PA6', 'PA7',
    'PA8', 'PA9', 'PA10','PA11','PA12','PA13','PA14','PA15',
	'PB0', 'PB1', 'PB2', 'PB3', 'PB4', 'PB5', 'PB6', 'PB7',
    'PB8', 'PB9', 'PB10','PB11','PB12','PB13','PB14','PB15',
	'PC13', 'PC14' , 'PC15'));
IOMode  = Enum('IOMode', (
    'OUTPUT', 'OUTPUT_OPEN_DRAIN', 'INPUT', 'INPUT_ANALOG', 'INPUT_PULLUP',
    'INPUT_PULLDOWN', 'INPUT_FLOATING', 'PWM', 'PWM_OPEN_DRAIN'));
IOLevel = Enum('IOLevel', ('LOW', 'HIGH'));

IONameLayout = ('PB11', 'PB10', 'PB1', 'PB0', 'PA7', 'PA6', 'PA5', 'PA4', 'PA3', 'PA2',
    'PA1', 'PA0', 'PC15', 'PC14', 'PB12', 'PB13', 'PB14', 'PB15', 'PA8', 'PA9',
    'PA10', 'PA15', 'PB3', 'PB4', 'PB5', 'PB6', 'PB7', 'PB8', 'PB9');
IOModeLayout=('INPUT', 'OUTPUT');
IOLevelLayout = ('LOW', 'HIGH');

class Gump_Uart(object):
    serial = 0;

    def __init(self):
        pass
    
    def openPort(self, name, bps):
        self.serial = serial.Serial(name, bps, timeout=0.5);

    def readline(self):
        return self.serial.readline();
    
    def writeData(self, str):
        self.serial.write(str);

class Gump_Arduino(Gump_Uart):
    def sendCMD(self, data):
        buf = [5, 0, 0, 0, 0, 0];
        ilen = len(data);
        for i in range(ilen):
            buf[i+1] = data[i];
        buf[5] = sum(buf);
        # print(buf);
        outdata = array('B', buf);
        self.writeData(outdata);
        # return self.readline();
    
    def getName(self):
        out = (SendCMD['getName'].value,);
        self.sendCMD(out);
        return self.readline();
    
    def getIoSize(self):
        out = (SendCMD['ioSize'].value,);
        self.sendCMD(out);
        return self.readline();

    def setBoardInit(self):
        out = (SendCMD['init'].value, IOMode['OUTPUT'].value-1, 0, IOLevel['HIGH'].value-1);
        self.sendCMD(out);
        return self.readline();
    
    def setIOMode(self, io, mode):
        out = (SendCMD['setMode'].value, IOMode[mode].value-1, IOName[io].value-1);
        self.sendCMD(out);
        return self.readline();

    def setIoLevel(self, io, level):
        out = (SendCMD['setLevel'].value, 0, IOName[io].value-1, IOLevel[level].value-1);
        self.sendCMD(out);
        return self.readline();

    def getIoLevel(self, io):
        out = (SendCMD['getLevel'].value, 0, IOName[io].value-1);
        self.sendCMD(out);
        return self.readline();

if __name__ == '__main__':
    port_list = getPCserial();
    if len(port_list) <= 0:
        print("The Serial port can't find");
    else:
        for i in port_list:
            print(i[0]);
    bridge = Gump_Arduino();
    bridge.openPort(SERPATH, SERBPS);
    print(bridge.getName());
    print(bridge.getIoSize());
    # print(bridge.setBoardInit());

    # 以轮询的方式进行初始化
    for  i in IONameLayout:
        print(i, end=' ');
        print(bridge.setIOMode(i, 'OUTPUT'));

    for i in IONameLayout:
        print(bridge.setIoLevel(i, 'LOW'));
        time.sleep(1);
        print(bridge.setIoLevel(i, 'HIGH'));

    print(bridge.setIOMode('PB5', 'INPUT'));
    print(bridge.getIoLevel('PB5'));