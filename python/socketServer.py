#/usr/bin/python
# -*- coding:utf-8 -*-

import socket;

sk = socket.socket();
hostname = socket.gethostname();
PORT = 18000;
address = (socket.gethostbyname(hostname), PORT);
print("地址:" + socket.gethostbyname(hostname));

sk.bind(address);
sk.listen();

while True:
    print('等待客户端链接' + '.'*20);
    connect, client = sk.accept();
    print(str(client) + '上线');
    while True:
        try:
            inp = input('>>>请输入:');
            connect.send(bytes(inp, 'utf8'));
            data = connect.recv(1024);
            if not data: break;
            print(str(data, 'utf8'));
        except ConnectionResetError:
            print('-'*10+'对方离线'+'-'*10);
            break;

sk.close();