from flask import Flask, url_for, request;
app = Flask(__name__)

@app.route('/hello')
def hello():
    return 'Hello Gump!';

@app.route('/bridgeArduino', methods=['GET', 'POST'])
def bridgeArduino():
    if request.method == 'POST':
        return do_send_post();
    else:
        return 'Bridge Arduino Test!';

with app.test_request_context():
    print(url_for('hello'));
    print(url_for('bridgeArduino'));

def do_send_post():
    pass;