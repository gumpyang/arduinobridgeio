#/usr/bin/python
# -*- coding:utf-8 -*-

import socket;

sk = socket.socket();
hostname = socket.gethostname();
PORT = 18000;
try:
    print("地址:"+hostname);
    sk.connect((socket.gethostbyname(hostname), PORT));
except ConnectionRefusedError:
    print('-'*10+'服务器未上线，或者不存在'+'-'*10);
    exit();

while True:
    try:
        data = sk.recv(1024);
    except ConnectionResetError:
        print('-'*10+'服务器歇菜了'+'-'*10);
        break;
    print(str(data, 'utf8'));
    inp = input('>>>请输入:');
    if inp == 'exit':
        break;
    sk.send(bytes(inp, 'utf8'));

sk.close()