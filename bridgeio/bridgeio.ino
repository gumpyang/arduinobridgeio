#define SERIAL_BAUD   115200

// #define DUEMILANOVE_BOARD   1
#define STM32F103_BOARD     1

#ifdef DUEMILANOVE_BOARD
#define LED           13
#define SelfInfoStr   "Arduion DUE"
#define IONUMBER      23
const byte io_name_array[IONUMBER] = {
  PC6, PD0, PD1, PD2, PD3, PD4,      PB5, PB4, PB3, PB2, PB1,
  PC5, PC4, PC3, PC2, PC1, PC0, PB6, PB7, PD5, PD6, PD7, PB0,
};

#elif defined(STM32F103_BOARD)

#define LED           PC13
#define SelfInfoStr   "STM32F103"
#define IONUMBER      31

/*
 * PA0, PA1, PA2, PA3, PA4, PA5, PA6, PA7, PA8, PA9, PA10, PA11, PA12, PA13,PA14,PA15,
 * PB0, PB1, PB2, PB3, PB4, PB5, PB6, PB7, PB8, PB9, PB10, PB11, PB12, PB13,PB14,PB15,
 * PC13 <-> LED
 * PA11 <-> USB-
 * PA12 <-> USB+
 */
const byte io_name_array[IONUMBER] = {
/*GND,  GND,  3.3V, RST,*/ PB11, PB10, PB1,    PB0, PA7,    PA6, PA5, PA4, PA3, PA2, PA1, PA0, PC15, PC14, /* PC13, VBAT */
  PB12, PB13, PB14, PB15,   PA8,  PA9, PA10,/* A11, A12 */ PA15, PB3, PB4, PB5, PB6, PB7, PB8, PB9, /* 5V, GND 3.3V */
  PA13, PA14,
};

#endif
#define LED_ON        LOW
#define LED_OFF       HIGH

/*
 * len+mode+io+level+checksum
 */
typedef struct {
  byte  len;
  byte  cmd;
  byte  mode;
  byte  io;
  byte  level;
  byte  checksum;
} SerialDataPackPar;

enum {
  SERIAL_GET_NAME = 1,
  SERIAL_GET_IO_NUMBER,
  SERIAL_SET_INIT,
  SERIAL_SET_MODE,
  SERIAL_SET_IO_LEVEL,
  SERIAL_GET_IO_LEVEL,
  SERIAL_CMD_MAX_NUMBER,
};

byte io_cmd_array[SERIAL_CMD_MAX_NUMBER] = {
  SERIAL_GET_NAME, SERIAL_GET_IO_NUMBER, SERIAL_SET_INIT, SERIAL_SET_MODE, SERIAL_SET_IO_LEVEL, SERIAL_GET_IO_LEVEL
};


SerialDataPackPar serial_rx = {0};
SerialDataPackPar serial_tx = {0};

void setup() {
  // put your setup code here, to run once:
  Serial.begin(SERIAL_BAUD);
  Serial.setTimeout(100);
  serial_rx.len = 0;
  serial_tx.len = 0;
  Serial.println(SelfInfoStr);
  pinMode(LED, OUTPUT);
  digitalWrite(LED, LED_OFF);
}

void loop() {
  // put your main code here, to run repeatedly:
  if (Serial.available() > 0) {
    digitalWrite(LED, LED_ON);
    serial_rx.len = Serial.read();
    Serial.readBytes((char*)&serial_rx.cmd, serial_rx.len);
    switch (serial_rx.cmd) {
      case SERIAL_GET_NAME:
        Serial.println(SelfInfoStr);
        break;
      case SERIAL_GET_IO_NUMBER:
        Serial.print("IO number ");
        Serial.println(IONUMBER, DEC);
        break;
      case SERIAL_SET_IO_LEVEL:
        digitalWrite(serial_rx.io, serial_rx.level);
        Serial.print("set io=");
        Serial.print(serial_rx.io, DEC);
        Serial.print(" level=");
        Serial.println(serial_rx.level, DEC);
        break;
      case SERIAL_SET_MODE:
#ifdef STM32F103_BOARD
        pinMode(serial_rx.io, (WiringPinMode)serial_rx.mode);
#endif
        Serial.print("set io=");
        Serial.print(serial_rx.io, DEC);
        Serial.print(" mode=");
        Serial.println(serial_rx.mode, DEC);
        break;
      case SERIAL_GET_IO_LEVEL:
        digitalRead(serial_rx.io);
        Serial.print("get io=");
        Serial.print(serial_rx.io, DEC);
        Serial.print(" level=");
        Serial.println(serial_rx.io, DEC);
        break;
    }
    Serial.flush();
    digitalWrite(LED, LED_OFF);
  }
}
